package com.epam.edu.online;

import com.epam.edu.online.pizzadevelop.factory.PizzaFactory;
import com.epam.edu.online.pizzadevelop.model.City;
import com.epam.edu.online.pizzadevelop.model.Pizza;
import com.epam.edu.online.pizzadevelop.model.PizzaType;

public class Application {

    public static void main(String[] args) {
        Pizza pizza1 = PizzaFactory.createPizza(PizzaType.CHEESE, City.KYIV);
        Pizza pizza2 = PizzaFactory.createPizza(PizzaType.CLAM, City.LVIV);
        Pizza pizza3 = PizzaFactory.createPizza(PizzaType.PEPPERONI, City.DNIPRO);
        showAllSteps(pizza1, pizza2, pizza3);
    }

    private static void showAllSteps(Pizza... pizzas) {
        for (Pizza pizza :pizzas) {
            pizza.prepare();
            pizza.bake();
            pizza.cut();
            pizza.box();
            System.out.println();
        }
    }

}
