package com.epam.edu.online.pizzadevelop.model;

public enum PizzaType {
    CHEESE, PEPPERONI, CLAM, VAGGIE
}
