package com.epam.edu.online.pizzadevelop.model.impl;

import com.epam.edu.online.pizzadevelop.model.City;
import com.epam.edu.online.pizzadevelop.model.Pizza;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PizzaPepperoni implements Pizza {
    private final static Logger log = LogManager.getLogger(PizzaPepperoni.class);
    private final City city;
    public PizzaPepperoni(City city) {
        this.city = city;
    }

    private final String name = "pepperoni pizza";

    @Override
    public void prepare() {
        log.info("prepare to do '" + name + "' with spice proportion " + city.getProportion());
    }

    @Override
    public void bake() {
        log.info("bake '" + name + "'");
    }

    @Override
    public void cut() {
        log.info("cut '" + name + "' to " + city.getPart() + " part");
    }

    @Override
    public void box() {
        log.info("boxed '" + name + "' in " + city.getCityName());
    }
}
