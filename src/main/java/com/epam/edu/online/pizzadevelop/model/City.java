package com.epam.edu.online.pizzadevelop.model;

public enum City {
    LVIV("Lviv", 1.05, 8),
    KYIV("Kyiv", 0.9, 12),
    DNIPRO("Dnipro", 0.95, 16);

    City(String cityName, double proportion, int part) {
        this.cityName = cityName;
        this.proportion = proportion;
        this.part = part;
    }

    private String cityName;
    private double proportion;
    private int part;

    public String getCityName() {
        return cityName;
    }
    public double getProportion() {
        return proportion;
    }
    public int getPart() {
        return part;
    }


}
