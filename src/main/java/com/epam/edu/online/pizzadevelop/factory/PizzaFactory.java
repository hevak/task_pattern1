package com.epam.edu.online.pizzadevelop.factory;

import com.epam.edu.online.pizzadevelop.model.City;
import com.epam.edu.online.pizzadevelop.model.Pizza;
import com.epam.edu.online.pizzadevelop.model.PizzaType;
import com.epam.edu.online.pizzadevelop.model.impl.PizzaCheese;
import com.epam.edu.online.pizzadevelop.model.impl.PizzaClam;
import com.epam.edu.online.pizzadevelop.model.impl.PizzaPepperoni;
import com.epam.edu.online.pizzadevelop.model.impl.PizzaVaggie;

public abstract class PizzaFactory {

    public static Pizza createPizza(PizzaType type, City city) {
        Pizza pizza = null;
        if (type == PizzaType.CHEESE) {
            pizza = new PizzaCheese(city);
        } else if (type == PizzaType.CLAM) {
            pizza = new PizzaClam(city);
        } else if (type == PizzaType.PEPPERONI) {
            pizza = new PizzaPepperoni(city);
        } else if (type == PizzaType.VAGGIE) {
            pizza = new PizzaVaggie(city);
        }
        return pizza;
    }
}
