package com.epam.edu.online.pizzadevelop.model;

public interface Pizza {
    void prepare();
    void bake();
    void cut();
    void box();
}
